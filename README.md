
# box-tabs-app
It is a simple application that can display three tabas with diferent contents. First tab - "files" retrevie content form the attached file from disk. There you can find two tables contains hostname highest traffic and most requested files. Second tab contains data from rss file. The last tabs display parsed data from json file.
 
## Requirements
- Installed Node.js
 
## Used technologies and libraries
- Node.js (Nodemon)
- Express
- Lodash
- Angular.js (ngRoute, ngAnimate)
- HTML5
- CSS3 (Sass, modernizr, normalize.css)
- Npm
- Gulp
- Bower

## Installation
In terminal go to the project path and use command: 
```sh
$ npm run init 
```

This will install globally Bower and Gulp. Then it will install all dependencies contained in packaage.json and bower.json. After that all styles will compile to css file.

Then start server 

```sh
$ npm run start
```

That's all. Open your browswer and load http://localhost:3000

## Development

If you want to develop you can use Nodemon to start and restart automatically node server. Nodemon monitor any changes and restart your server. For using nodemon you have to install it by npm or just use command:

```sh
$ npm run initDev 
```

This will install all needed things(bower and all dependencies) and also Nodemon. To start Nodemon you can use:

```sh
$ npm run startDev
```

or 

```sh
$ nodemon server.js
```

If you don't need auto restart server, you can use standard method for start server.

Application structure:

```sh
- app
    ----- routes.js <!-- contains app routes -->
    - node_modules <!-- created by npm install -->
    - public <!-- all frontend and angular files -->
    ----- css <!-- compilated sass diles -->
    ----- fonts <!-- fonts  -->
    ----- js 
    ---------- controllers <!-- angular controllers -->
    ---------- services <!-- angular services -->
    ---------- app.js <!-- angular application with routes -->
    ----- libs <!-- created by bower install -->
    ----- views 
    ----------  partials <! -- partials view include to index.html -->
    ---------- index.html <!-- main html -->
    - .bowerrc <!-- tells bower where to put files (public/libs) -->
    - .gitignore <!-- tells git wich file will be untracked -->
    - bower.json <!-- tells bower which files app need -->
    - gulpfile.js <!-- build system automating tasks -->
    - nodemon.json <!-- tells nodemon wich file will be unwatched -->
    - package.json <!-- tells npm which packages app need -->
    - server.js <!-- set up node app -->
    - varnish.log <!-- file with data  -->
```


App for build using gulp, there are a few task you can use:

To compile sass file use: 

```sh
$ gulp styles
```

To watch sass files use: 

```sh
$ gulp watch
```

If you want preapre projet for the production site, use:

```sh
$ gulp dist
```

It will create dist forlder contains all html files with concatenated and minified js and css.

## Usage
- Install project
- Open your favourite browswer and load http://localhost:3000  to see the working app

## TODO 
- write tests

## License
MIT
